# About the project

## The project only includes the manifest file to depploy an application on GKE cluster using Argo CD
* Web application helps customers can montinor their device status on our cloud
* The manifest files of application stored on Gitlab repository 
* Deploy application using Argo CD to help developers manually or automatically sync the live state with the desired state.
 
  ![image](/uploads/75bf1bdf03378d32e0156febd4b3f2b4/image.png)

## Install Argo CD
```
  kubectl create namespace argocd
  kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
```

 * After installation, make sure all the Argo CD components has been deployed on the cluster

  ![image](/uploads/0fd90339781178e487bcfeddc0c2ac7e/image.png)

## Expose the Argo CD API Server 
* By default, the Argo CD API server is not exposed with an external IP. To access the API server, you chan change the service of Argo CD API server to LoadBalancer type

  `kubectl patch svc argocd-server -n argocd -p '{"spec": {"type": "LoadBalancer"}}`

* The initial password for the admin account is auto-generated and stored as clear text in the field password in a secret named argocd-initial-admin-secret in your Argo CD installation namespace. You can simply retrieve this password using kubectl:

  `kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d; echo`

## Connect to Argo CD portal 
 * You can access the Argo CD portal via LoadBalancer IP address, enter "admin" and above password to login 

 ![image](/uploads/36c50e5a969e355cbd5d203cc7a36bcb/image.png) 

## Connect to GitLab Repo using https
* For Gitlab repos you have to pass the full repo name including .git. like https://gitlab.com/group/repo.git
* Here is an sample of the application on Argo CD. You can select Sync policy to auto/manualy sync your manefest file on the repository 

  ![image](/uploads/3f3bc84af02c75aa273bc034bd40b0b3/image.png)

